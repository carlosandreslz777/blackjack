var tipoCarta = ["spades", "diams", "clubs", "hearts"];
var valor = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
var puntos = {"2":2, "3":3, "4":4, "5":5, "6":6, "7":7, "8":8, "9":9, "10":10, "J":10, "Q":10, "K":10, "A":11};

class Maso{

    constructor(){
        this.maso = new Array();
        for(let i = 0; i < tipoCarta.length; i++){
            for(let j = 0; j < valor.length; j++){
                let carta = {Value: valor[j], Suit: tipoCarta[i], puntos: puntos[valor[j]]};
                this.maso.push(carta);
            }
        }
    }

    //Mezclar cartas para repartir
    barajar(){
        let i, j, tmp;
        for (i = this.maso.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            tmp = this.maso[i];
            this.maso[i] = this.maso[j];
            this.maso[j] = tmp;
        }
    }

    carta(i){
        return this.maso[i];
    }

    getLength(){
        return this.maso.length;
    }

    pop(){
        return this.maso.pop();
    }

    push(carta){
        return this.maso.push(carta);
    }

    // devolver el maso
    getArray(){
        return this.maso;
    }
}

exports.tipoCarta = tipoCarta;
exports.valor = valor;
exports.puntos = puntos;
exports.Maso = Maso;