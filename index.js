const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const path = require('path');

// Valores de cartas
const Maso = require('./ModuloMaso').Maso;

app.use(express.static(path.join(__dirname, 'public')));

var jugadores = [];
var sala1jugadores = [];
var sala1Turnos = 0;
var masoJuego;

// Banderas
var j1Turno = true;
var j1Bool = true;
var proxTurno = false;
var finJuego = false;

setInterval(function(){0
    console.log(sala1Turnos);
}, 3000);

io.on('connection', socket => {
    //console.log(socket.id + " joined.");
    jugadores.push(socket.id);
    //console.log(jugadores);

    // Recibe nombre de jugador y espera que  se pueda unir .
    socket.on('agregar nombre', nombre => {
        socket.nombre = nombre;
    });

    socket.on('solicitud sala 1', () => {
        console.log(socket.id + " solicitar unirse a la sala 1");

        //Validación de que solo reciba dos jugadores 
        if(sala1Turnos < 2){
            socket.join('sala 1', () => {
                /* console.log(socket.id + " se ingresa a la sala.");
                console.log("Sala 1 object:");
                console.log(io.sockets.adapter.rooms['sala 1']); */
                sala1Turnos = io.sockets.adapter.rooms['sala 1'].length;
                // Agregar jugador a la lista de salas (suponiendo que aún no estén en ella)
                if(enSala(socket, sala1jugadores)){
                    console.log("Intento de reunirse a una sala " + socket.id);
                }
                else{
                    socket.puntajeJuego = 0;
                    sala1jugadores.push(socket);
                }
                socket.emit('Estoy Listo');
                if(sala1Turnos == 2){
                    // Se conecta el segundo jugador 
                    io.to('sala 1').emit('inicio juego');
                    iniciaJuego(sala1jugadores);
                }
            });
        }
        else{

            console.log(socket.id + " La sala esta llena.");
            socket.emit('sala llena');
        }
    });

    // Pedir
    socket.on('Pedir', () => {
        if(socket.p1 == j1Turno){
            Pedir(socket);
            if(!obtenerOponente(socket, sala1jugadores).sePlanta){
                j1Turno = !j1Turno;
            }
            actualizarTurno(sala1jugadores);
        }
        else{
            console.log(socket.nombre + " intento de pedir carta sin tener el turno. ");
        }
        checkearFinJuego(sala1jugadores);
    });
    // Plantarse
    socket.on('Plantarse', () => {
        if(socket.p1 == j1Turno){
            Plantarse(socket);
            j1Turno = !j1Turno;
            actualizarTurno(sala1jugadores);
        }
        
        else{
            console.log(socket.nombre + " intento de plantarse pero no es el turno.");
        }
        checkearFinJuego(sala1jugadores);
    });
    // Volver a jugar . 
    socket.on('listo', () => {
        console.log(socket.id + " se esta esperando reanudar.");
        socket.estaListo = true;
        if(chekearLista(sala1jugadores)){
            console.log("los jugadores estan listos...");
            io.to('sala 1').emit('restart');
            iniciaJuego(sala1jugadores);
        }
    });
    // Desconexion 
    socket.on('disconnect', discMsg => {
        console.log(socket.id + " disconnected.");
        // Se elimina el jugador de conectado de la poblacion actual
        sala1jugadores = sala1jugadores.filter(player => player.id == socket.id);
        sala1Turnos = sala1jugadores.length;
    });
});

// Actualización de cada vez que se plantea o se pide carta
function actualizarTurno(socketList){
    for(var i = 0; i < socketList.length; i++){
        io.to(socketList[i].id).emit('actualizar turno', socketList[i].p1 == j1Turno);
    }
}

// Determinar si los dos jugadores estan listos para volver a jugar.
function chekearLista(socketList){
    for(var i = 0; i < socketList.length; i++){
        if(!socketList[i].estaListo){
            return false;
        }
    }
    return true;
}

// inicio de juego .
function iniciaJuego(jugadores){
    console.log("Inicia Juego...");
    masoJuego = new Maso();
    masoJuego.barajar();
    proxTurno = false;
    j1Turno = true;
    finJuego = false;

    jugadores.forEach((jugador) => {
        jugador.puntaje = 0;
        jugador.aces = 0;
        jugador.sePlanta = false;
        jugador.sePasa = false;
        jugador.estaListo = false;
        
        jugador.baraja = new Array();
        Pedir(jugador, 2);
    });

    // Designación de turnos 
    j1Bool = !j1Bool;
    jugadores[0].p1 = j1Bool;
    jugadores[1].p1 = !j1Bool;

    // aviso de quien tiene turno.
    jugadores[0].emit('inicia turno', jugadores[0].p1);
    jugadores[1].emit('inicia turno', jugadores[1].p1);

    emitirPuntaje(jugadores);
}

// Obtener datos de componente .
function obtenerOponente(jugador, listaSocket){
    console.log("Obtener las cartas del openente: " + listaSocket.length);
    for(var i = 0; i < listaSocket.length; i++){
        if(listaSocket[i].p1 === !jugador.p1){
            return listaSocket[i];
        }
    }
}

function paraTodo(listaSocket){
    for(var i = 0; i < listaSocket.length; i++){
        if(!listaSocket[i].sePlanta){
            return false;
        }
    }
    return true;
}

function alguienSePlanto(listaSocket){
    for(var i = 0; i < listaSocket.length; i++){
        if(listaSocket[i].sePlanta){
            return true;
        }
    }
    return false;
}

function cualquieraFalla(listaSocket){
    for(var i = 0; i < listaSocket.length; i++){
        if(listaSocket[i].sePasa){
            return true;
        }
    }
    return false;
}

function todosFallan(listaSocket){
    for(var i = 0; i < listaSocket.length; i++){
        if(!listaSocket[i].sePasa){
            return false;
        }
    }
    return true;
}


function checkearFinJuego(listaSocket){
    console.log("comprobando el ultimo turno: " + proxTurno);
    if(proxTurno){
        // turno ed ambos jugadores 
        if(todosFallan(listaSocket)){
            console.log("turno acpetado");
            finJuego = true;
            emitirEmpate(listaSocket);
        }
        // Jugador se pasa y el otro no se pasa
        else{
            console.log("se paso");
            finJuego = true;
            let ganador = listaSocket.filter(player => !player.sePasa)[0];
            emitirGanadorPerdedor(ganador, listaSocket);
            console.log("ganador  id: " + ganador.id);
        }
    }
    if(paraTodo(listaSocket)){
        let resultGanador = determinarGanador(listaSocket);
        // Caso de empate 
        if(resultGanador == "empate"){
            finJuego = true;
            emitirEmpate(listaSocket);
        }
        // Ambos jugadores se plantan
        else if(typeof(resultGanador) == "number"){
            
            finJuego = true;
            let indexGanador = resultGanador;
            let ganador = listaSocket[indexGanador];
            emitirGanadorPerdedor(ganador, listaSocket);
        }
    }
    // si se pasa y el otro jugador ya se planto
    if(cualquieraFalla(listaSocket)){
        if(alguienSePlanto(listaSocket)){
            finJuego = true;
            let ganador = listaSocket.filter(player => !player.sePasa)[0];
            console.log("ganador id: " + ganador.id);
            emitirGanadorPerdedor(ganador, listaSocket);
        }
        // Se pasa cuando el otro jugador no se planta
        else{
            proxTurno = true;
        }
    }
}

// Se determina el ganador de un enfrentamiento .
function determinarGanador(listaSocket){
    // Cuandi la sala esta vacia
    if(listaSocket.length == 0){
        console.log("Error no se puede determinar ganador listaSocket vacio .");
        return;
    }
    let empateActual = true;
    for(var i = 1; i < listaSocket.length; i++){
        if(listaSocket[i].puntaje != listaSocket[i-1].puntaje){
            empateActual = false;
            break;
        }
    }
    if(empateActual){
        return "empate";
    }
    // Devuelve indice de la listaSocket
    let ganadorActual = 0;
    for(var i = 1; i < listaSocket.length; i++){
        if(listaSocket[i].puntaje > listaSocket[ganadorActual].puntaje){
            ganadorActual = i;
        }
    }
    console.log("Ganador : " + listaSocket[ganadorActual].id);
    return ganadorActual;
}

function emitirEmpate(listaSocket){
    listaSocket[0].puntajeJuego += 0.5;
    listaSocket[1].puntajeJuego += 0.5;
    io.to(listaSocket[0].id).emit('empate', listaSocket[1].baraja);
    io.to(listaSocket[1].id).emit('empate', listaSocket[0].baraja);
    emitirPuntaje(listaSocket);
}

function emitirGanadorPerdedor(ganador, listaSocket){
    ganador.puntajeJuego++;
    let perdedor = obtenerOponente(ganador, listaSocket);
    io.to(ganador.id).emit('ganador', ganador.baraja);
    io.to(perdedor.id).emit('perdedor', perdedor.baraja);
    emitirPuntaje(listaSocket);
}

function emitirPuntaje(listaSocket){
    //marcador de posiciones  y resultado. 
    let p1 = listaSocket[0];
    let p2 = listaSocket[1];
    let p1MS = `${p1.nombre}: ${p1.puntajeJuego} | ${p2.nombre}: ${p2.puntajeJuego}`;
    let p2MS = `${p2.nombre}: ${p2.puntajeJuego} | ${p1.nombre}: ${p1.puntajeJuego}`;

    let p1S = `Tu puntaje: ${p1.puntaje} &nbsp;&nbsp; | &nbsp;&nbsp; Puntaje oponente: ${p2.puntaje}`;
    let p2S = `Tu puntaje: ${p2.puntaje} &nbsp;&nbsp; | &nbsp;&nbsp; Puntaje oponente: ${p1.puntaje}`;
    
    // se envian los scores  a los jugadores cuando ya se acaba partida.
    if(finJuego){
        io.to(p1.id).emit('fin juego', {puntajeJuego: p1MS, puntajeTexto: p1S} );
        io.to(p2.id).emit('fin juego', {puntajeJuego: p2MS, puntajeTexto: p2S} );
    }
    else{
        io.to(p1.id).emit('puntaje juego', p1MS);
        io.to(p2.id).emit('puntaje juego', p2MS);
    }
}

function Pedir(jugador, numeroCartas = 1){
    // deal a card to jugador
    for(var i = 0; i < numeroCartas; i++){
        console.log("Card Pedir...");
        let nuevaCarta = masoJuego.pop();
        jugador.baraja.push(nuevaCarta);
        actualizarScore(jugador, nuevaCarta);
        // repartir una carta al jugador
        jugador.emit('Pedir', nuevaCarta);
        jugador.to('sala 1').emit('pedir oponente');
    }
}

function Plantarse(jugador){
    jugador.sePlanta = true;
    console.log(jugador.id + " Plantarses.")
    jugador.emit('Plantarse');
    jugador.to('sala 1').emit('plantarse oponente');
}

function actualizarScore(jugador, nuevaCarta){
    console.log("se ACTUALIZA puntuacion...");
    console.log("nueva carta: " + nuevaCarta.valor);
    // Si AL JUGADOR LE SALE UN AS
    if(nuevaCarta.valor === "A"){
        jugador.aces++;
    }
    // en caso de  (blackjack)
    if(jugador.puntaje + nuevaCarta.puntos == 21){
        console.log(jugador.id + " total 21 (blackjack)!");
    }
    // Jugador se pasarse
    else if(jugador.puntaje + nuevaCarta.puntos > 21){
        if(jugador.aces > 0){
            jugador.aces--;
            jugador.puntaje -= 10;
        }
        else{
            //Juego terminado
            console.log(jugador.id + " se Paso.");
            jugador.sePasa = true;
            jugador.emit('sePaso');
        }
    }
    jugador.puntaje += nuevaCarta.puntos;
    jugador.emit('actualizar puntaje', jugador.puntaje);
}

// verificar si un jugador esta o no en la sala
function enSala(jugador, listaSocket){
    for(var i = 0; i < listaSocket.length; i++){
        if(listaSocket[i].id == jugador.id){
            return true;
        }
    }
    return false;
}

// iniico del servidor 
const PORT = process.env.PORT || 2020;

server.listen(PORT, () => {
    console.log("Listening on port %d", PORT);
});