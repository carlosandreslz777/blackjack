const socket = io();
console.log("Inicia");

var puntajeJuego = 0;
var nombre = "";
var oppStood = false;

const myInput = document.getElementById("userName");
$('#userName').focus();

myInput.addEventListener("keyup", function(event) {
	
	if (event.keyCode === 13) {
	  document.getElementById('idLabelJugar').className = "ocultarTexto";
	  event.preventDefault();
	  nombreJugado();
	}
});
  
// agregar el nombre del jugador 
function nombreJugado(){
	nombre = document.getElementById('userName').value;
	socket.emit('agregar nombre', nombre);
	$('#primero').fadeOut();
	$('#segundo').show();
	
}

function mostrarOponente(baraja_oponente){
	let oppHandDiv = document.getElementById('barajaOponente');
	oppHandDiv.innerHTML = '';
	baraja_oponente.forEach(cartaOponente => {
		let card = document.createElement("div");
		let spanRank = document.createElement("span");
		spanRank.innerHTML = cartaOponente.Value;
		spanRank.className = "rank";
		let spanSuit = document.createElement("span");
		spanSuit.innerHTML = `&${cartaOponente.Suit};`;
		spanSuit.className = "suit";
		card.appendChild(spanRank);
		card.appendChild(spanSuit);
		card.className = `card rank-${cartaOponente.Value} ${cartaOponente.Suit}`;
		oppHandDiv.appendChild(card);
	});
}


function accionBotones(turnoActual){
	let buttons = Array.from(document.getElementsByClassName('acciones'));
	buttons.forEach(btn => {
		console.log("se deshabilita jugador: " + turnoActual);
		if(turnoActual){
			btn.removeAttribute('disabled');
		}
		else{
			btn.setAttribute('disabled', "");
		}
	});
}


function solicitudSala(){
	document.getElementById('btn_listo').className='ocultar_boton';
	document.getElementById('id_carta').className='carta2';
	document.getElementById('idcargandotxt').className='txtCargando2';
	socket.emit('solicitud sala 1');
}

function jugarNuevamente(){
	socket.emit('listo');
	document.getElementById('restart').classList.add('readied');
}

function pedirCarta(){
	socket.emit('Pedir');
}

function plantarseAqui(){
	socket.emit('Plantarse');
}

// Recibiendo de index.js

socket.on('actualizar turno', turnoActual => {
	accionBotones(turnoActual);
});

socket.on('inicio juego', () => {
	$('#segundo').hide();
	$('#third').fadeIn();
});

socket.on('sala llena', () => {
	document.getElementById('lobbyMsg').textContent = "La sala ya esta llena.";
});

// Reinicio de juego
socket.on('restart', () => {
	// se restablece divs al estado inicial
	console.log("Se reinicia el juego");
	document.getElementById('mensajeTerminarJuego').innerHTML = "";
	document.getElementById('baraja').innerHTML = "";
	document.getElementById('barajaOponente').innerHTML = "";
	// restablecer visiblidad del boton. 
	document.getElementById('restart').style.display = 'none';
	document.getElementById('restart').classList.remove('readied');
	oppStood = false;
});

socket.on('fin juego', puntajeJuegoObj => {
	console.log(' evento de Juego recibido...');
	document.getElementById('nombreJugador2').textContent = puntajeJuegoObj.puntajeJuego;
	document.getElementById('puntaje_div').innerHTML = puntajeJuegoObj.puntajeTexto;
	document.getElementById('restart').style.display = 'inline-block';
	accionBotones(false);
});

socket.on('puntaje juego', puntajeJuego => {
	console.log('evento de puntacion recibido...', puntajeJuego);
	let nombreJugador2 = document.getElementById('nombreJugador2');
	nombreJugador2.textContent = puntajeJuego; 
});

socket.on('empate', barajaOponente => {
	console.log("evento de empate");
	mostrarOponente(barajaOponente);
	document.getElementById('mensajeTerminarJuego').innerHTML = "Empate!";
	document.getElementById('mensajeTerminarJuego').style.color = "blue";
	document.getElementById('turno_div').textContent = "";
});

socket.on('ganador', barajaOponente => {
	console.log("Evento de ganador ");
	mostrarOponente(barajaOponente);
	document.getElementById('mensajeTerminarJuego').innerHTML = "Tu Eres el Ganador !Crack!!";
	document.getElementById('mensajeTerminarJuego').style.color = "green";
	document.getElementById('turno_div').textContent = "";
})

socket.on('perdedor', barajaOponente => {
	console.log("evento de perdedor");
	mostrarOponente(barajaOponente);
	document.getElementById('mensajeTerminarJuego').innerHTML = "Perdiste!";
	document.getElementById('mensajeTerminarJuego').style.color = "red";
	document.getElementById('turno_div').textContent = "";
})

socket.on('Estoy Listo', () => {
	console.log("has ingresado satisfactoriamente a la sala 1.");
});

socket.on('inicia turno', turnoActual => {
	
	console.log('evento turno de inicio');
	accionBotones(turnoActual);
	
});

socket.on('Pedir', newCard =>{
	console.log("Evento de pedir con la carta:", newCard);
	let handDiv = document.getElementById('baraja');
	let card = document.createElement("div");
	let spanRank = document.createElement("span");
	spanRank.innerHTML = newCard.Value;
	spanRank.className = "rank";
	let spanSuit = document.createElement("span");
	spanSuit.innerHTML = `&${newCard.Suit};`;
	spanSuit.className = "suit";
	card.appendChild(spanRank);
	card.appendChild(spanSuit);
	card.className = `card rank-${newCard.Value} ${newCard.Suit}`;
	handDiv.appendChild(card);
	
});

socket.on('pedir oponente', () => {
	let oppHandDiv = document.getElementById('barajaOponente');
	let card = document.createElement("div");
	card.className = "card back";
	oppHandDiv.appendChild(card);
	
});

socket.on('Plantarse', () => {
	
});

socket.on('plantarse oponente', () => {
	oppStood = true;
	
});

// objeto de puntaje de cada turno
socket.on('actualizar puntaje', score =>{
	console.log("score",score);
	let puntaje_div = document.getElementById('puntaje_div');
	puntaje_div.innerHTML = "Tu puntuación: " + score;
});